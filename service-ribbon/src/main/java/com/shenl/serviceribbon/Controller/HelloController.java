package com.shenl.serviceribbon.Controller;

import com.shenl.serviceribbon.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Shenl
 * @date 2020/9/15 16:54
 */
@RestController
public class HelloController {
    @Autowired
    HelloService helloService;

    @RequestMapping("hi")
    public Object hi(@RequestParam String name){
        return helloService.hiService(name);
    }
}
