package com.shenl.authserver.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Shenl
 * @date 2020/9/15 16:23
 */
@RestController
@RefreshScope
public class AuthController {

    @Value("${server.port}")
    String port;

    @Value("${who}")
    String who;

    /**
     * post /actuator/bus-refresh 刷新配置(访问本服务或config-server服务)
     *
     * @param name
     * @return
     */
    @RequestMapping("/hi")
    public String home(@RequestParam(value = "name", defaultValue = "shenl") String name) {
        return who;
    }
}
