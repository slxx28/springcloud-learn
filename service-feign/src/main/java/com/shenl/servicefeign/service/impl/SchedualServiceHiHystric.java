package com.shenl.servicefeign.service.impl;

import com.shenl.servicefeign.service.SchedualServiceHi;
import org.springframework.stereotype.Component;

/**
 * @author Shenl
 * @date 2020/9/15 17:34
 */
@Component
public class SchedualServiceHiHystric implements SchedualServiceHi {
    @Override
    public String sayHiFromClientOne(String name) {
        return "sorry," + name;
    }
}
